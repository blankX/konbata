package main

import (
	"os"
	"path/filepath"
)

func xdgDataHome() string {
	if s, ok := os.LookupEnv("XDG_DATA_HOME"); ok {
		return s
	}
	return filepath.Join(os.Getenv("HOME"), ".local", "share")
}
