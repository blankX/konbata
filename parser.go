package main

import (
	"encoding/xml"
	"strings"
	"time"

	"git.sr.ht/~adnano/go-gemini"
)

type Feed struct {
	XMLName xml.Name    `xml:"feed"`
	XMLNS   string      `xml:"xmlns,attr"`
	Title   string      `xml:"title"`
	Link    FeedLink    `xml:"link"`
	Updated string      `xml:"updated"`
	Id      string      `xml:"id"`
	Entries []FeedEntry `xml:"entry"`
}
type FeedLink struct {
	XMLName xml.Name `xml:"link"`
	Href    string   `xml:"href,attr"`
}
type FeedEntry struct {
	XMLName xml.Name  `xml:"entry"`
	Title   string    `xml:"title"`
	Link    EntryLink `xml:"link"`
	Id      string    `xml:"id"`
	Updated string    `xml:"updated"`
}
type EntryLink struct {
	XMLName xml.Name `xml:"link"`
	Href    string   `xml:"href,attr"`
	Rel     string   `xml:"rel,attr"`
}

type FeedItem struct {
	Date  time.Time
	Title string
	Link  string
}

type ByTime []FeedItem

func (a ByTime) Len() int {
	return len(a)
}
func (a ByTime) Less(i, j int) bool {
	return a[i].Date.Before(a[j].Date)
}
func (a ByTime) Swap(i, j int) {
	a[i], a[j] = a[j], a[i]
}

type AtomWriter struct {
	Title string
	Items []FeedItem
}

func (a *AtomWriter) Handle(line gemini.Line) {
	switch line := line.(type) {
	case gemini.LineLink:
		runes := []rune(line.Name)
		t, err := time.Parse("2006-01-02", string(runes[:10]))
		if err == nil {
			a.Items = append(a.Items, FeedItem{
				Date:  t,
				Title: strings.TrimSpace(strings.TrimLeft(strings.TrimSpace(string(runes[10:])), ":-—")),
				Link:  string(line.URL),
			})
		}
	case gemini.LineHeading1:
		if a.Title == "" {
			a.Title = string(line)
		}
	}
}
