package main

import (
	"bufio"
	"net/url"
	"os"
	"path/filepath"
	"strings"
)

var predirs map[string]*url.URL

func populatePRedirs() error {
	file, err := os.OpenFile(filepath.Join(xdgDataHome(), "konbata", "predirs"), os.O_RDONLY|os.O_CREATE, 0600)
	if err != nil {
		return err
	}
	defer file.Close()
	scanner := bufio.NewScanner(file)
	predirs = make(map[string]*url.URL)
	for scanner.Scan() {
		urls := strings.Split(scanner.Text(), " ")
		if len(urls) != 2 {
			continue
		}
		url0, err := url.Parse(urls[0])
		if err != nil {
			continue
		}
		if url0.Scheme != "gemini" {
			continue
		}
		url1, err := url.Parse(urls[1])
		if err != nil {
			continue
		}
		if url1.Scheme != "gemini" {
			continue
		}
		predirs[url0.String()] = url1
	}
	return nil
}

func savePRedirs() error {
	file, err := os.OpenFile(filepath.Join(xdgDataHome(), "konbata", "predirs"), os.O_WRONLY|os.O_CREATE, 0600)
	if err != nil {
		return err
	}
	defer file.Close()
	err = file.Truncate(0)
	if err != nil {
		return err
	}
	writer := bufio.NewWriter(file)
	for url0, url1 := range predirs {
		_, err = writer.WriteString(url0)
		if err != nil {
			return err
		}
		_, err = writer.WriteRune(' ')
		if err != nil {
			return err
		}
		_, err = writer.WriteString(url1.String())
		if err != nil {
			return err
		}
		_, err = writer.WriteRune('\n')
		if err != nil {
			return err
		}
	}
	return writer.Flush()
}
